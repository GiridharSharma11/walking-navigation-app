2/*
 *ENG1003 Assignment 2 (2018 S1)
 *Team 205
 *Members: Ngo Jin Yi 28654390
 *         Carwen Wong Kah Vun 29032083
 *         Jose Fernando 29062950
 *         Giridhar Gopal Sharma 29223709
 *
 *Walking Navigation App
 *Aim: This javascript file is designed to initialise a map to navigate the paths and show some related information of the paths that has been selected by the user.
 *Functions included in this file:
 *  -initMap(position)
 *  -drawPathLine()
 *  -getLocation(position)
 *  -error(err)
 *  -accuracyCircle()
 *  -storeUserMovement()
 *  -navProperties()
 *  -showNextDirection()
 *Last Modified: 19 May 2018
 */

//==============================================================================
//============================Navigate==Page====================================
//==============================================================================

//initializing navigation properties
let id,prevHeading,accuracy,currentPosition;
let previousPosition,pos,coordinates,distToDest;
let marker = null;
let userMarker = null;
let map = null;
let accuracyArea = null;
let pathLine = null;
let infoBox = null;

let centerPos ={
    lat:null,
    lng:null
};

let currPos ={
    lat:null,
    lng:null
};

let prevPos ={
    lat:null,
    lng:null
};

let trackHistory={
    latLng:[],
    timeStamp:[]
};

//=======================================================================================
//==============================Data==Retrieving=========================================
//=======================================================================================

//retrieving selected path index and path type from local storage
//path type : 0 is for "server" routes, 1 is for "user" routes
let pathChosen = localStorage.getItem(storageKey+"-selected");
let pathChosenParse = JSON.parse(pathChosen);
let pathType = pathChosenParse[0];
let pathIndex = pathChosenParse[1];
let pathlistData;

//retrieving available data from local storage
if (typeof(Storage) !== undefined)
    {
        let pathlistDataJSON = localStorage.getItem(storageKey);
        pathlistData = JSON.parse(pathlistDataJSON);
    }

//storing all "available" path coordinates into availablePath as an array
let availablePath;
let newPathlist = new Pathlist;
newPathlist.initializeFromPathlistPDO(pathlistData,pathType);
availablePath = newPathlist.pathArr;

//=========================================================================================
//====================================Map==Display=========================================
//=========================================================================================

//setting the center of the map displayed as the user current position
let center=navigator.geolocation.getCurrentPosition(initMap);
//callback function initMap to print google map into the page
function initMap(position)
{
    if (map === null)
        {
            //getting the current location of the user and setting it as the first position
            centerPos.lat=position.coords.latitude;
            centerPos.lng=position.coords.longitude;
            let mapOptions = {
                center:{
                  lat: centerPos.lat,
                  lng: centerPos.lng
                },
                zoom:18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            //displaying google map in the page
            map = new google.maps.Map(document.getElementById("map"),mapOptions);
        }
    
    //initializing watchPosition method to track user's position
    id = navigator.geolocation.watchPosition(getLocation, error, options);
    
    //drawPathLine() is mainly to draw the polyline of the particular selected Path
    //this function is also to set markers along every waypoint
    drawPathLine();
}

function drawPathLine()
{
    //extracting the selected path's set of coordinates
    let pathCoordinates = availablePath[pathIndex];
    
    //if pathLine(polyline that draw the path) is still null(not printed) in the map,
    //it defines the path line
    if (pathLine === null)
        {
            pathLine = new google.maps.Polyline({
                path: pathCoordinates,
                geodesic: true,
                strokeColor: 'black',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
        }
    //else if pathLine is already in the map, it sets the existing into null, and defines a new one
    else if (pathLine !== null)
        {
            pathLine.setMap(null);
            pathLine = new google.maps.Polyline({
                path: pathCoordinates,
                geodesic: true,
                strokeColor: 'black',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
        }
    
    //displaying the path line in the map
    pathLine.setMap(map);
    
    //displaying markers in every coordinate of the chosen path
    for (let i=0; i<pathCoordinates.length; i++)
    {
        marker = new google.maps.Marker({
            position: pathCoordinates[i],
            map: map,
        });
    }
}

//=================================================================================================
//====================================Navigation==Section==========================================
//=================================================================================================

//getLocation function as the first parameter of watchPosition method
//getLocation function is executed once watchPosition method is called successfully
function getLocation(position)
{
    //tracking the current position of the user
    pos = position;
    currPos.lat=position.coords.latitude;
    currPos.lng=position.coords.longitude;
    currentPosition = new google.maps.LatLng(currPos);
    
    if (prevPos.lat===null && prevPos.lng===null)
        {
            //storing the first current position(parameter position) as previous position
            prevPos.lat = currPos.lat;
            prevPos.lng = currPos.lng;
            previousPosition = new google.maps.LatLng(prevPos);
            
            //calculating the heading at that certain position
            prevHeading = position.coords.heading;
            
            //calculating the accuracy radius of that certain position
            accuracy = position.coords.accuracy;
        }
    else 
        {
            //determining the heading between the previous and current position
            prevHeading = google.maps.geometry.spherical.computeHeading(previousPosition,currentPosition);
            
            //storing the current position as the previous position after user moves to the next position
            prevPos.lat = currPos.lat;
            prevPos.lng = currPos.lng;
            previousPosition = new google.maps.LatLng(prevPos);
            
            //calculating the accuracy radius of that certain position
            accuracy = position.coords.accuracy;
        }
    
    //setting marker of the current position as blue arrow that can rotate depending on the user heading
    if (userMarker === null)
        {
            userMarker = new google.maps.Marker({
                position: currentPosition,
                map: map,
                icon: {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 4,
                    fillColor: "blue",
                    fillOpacity: 0.8,
                    strokeWeight: 2,
                    //rotation properties
                    rotation: parseInt(prevHeading),
                    animation: google.maps.Animation.BOUNCE
                }
            });
        }
    else if (marker !== null)
        {
        //updating user position everytime user change direction and position
           userMarker.setMap(null);
            userMarker = new google.maps.Marker({
                position: currentPosition,
                map: map,
                icon: {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 4,
                    fillColor: "blue",
                    fillOpacity: 0.8,
                    strokeWeight: 2,
                    //rotation properties
                    rotation: parseInt(prevHeading),
                    animation: google.maps.Animation.BOUNCE
                }
            });
        }
    
    //initializing other functions
    //accuracyCircle() is to display the accuracy area inside the map
    accuracyCircle();
    
    //storeUserMovement() is to keep the history of the user's tracks
    storeUserMovement();

    //wayPoints() is mainly to show user the next waypoint
    //it also serves as most of the app features in the navigation page
    navProperties();

    //showNextWayPoint() is to display text and images indicating directions to take in the map
    showNextDirection();
}

//defining error function which is the second parameter of watchPosition method
function error(err) 
{
    displayMessage("Device's GPS is not working");
}

//defining optional feature that provides configuration options as the 3rd parameter of watchPosition method
let options = {
  enableHighAccuracy: true,
  timeout: 1000,
  maximumAge: 0
};

//========================================================================================================
//===========================================Several==Functions===========================================
//========================================================================================================

//============================================Accuracy==Circle============================================
function accuracyCircle()
{
    //setting accuracy circle area as null to avoid overlapping
    if (accuracyArea !== null)
    {
        accuracyArea.setMap(null);
        accuracyArea = null;
    }
    
    //executing the google API maps.Circle method using accuracy radius we have calculated
    if (accuracyArea === null)
    {
        accuracyArea = new google.maps.Circle({
            center: currentPosition,
            map: map,
            strokeColor: 'black',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: 'pink',
            fillOpacity: 0.35,
            radius: accuracy
        });
    }
}

//====================================Storing==Movement==History========================================
function storeUserMovement()
{
    //storing user movement and the corresponding time to trackHistory objects continuously
    let timestamp = pos.timestamp;
    trackHistory.latLng.push(currentPosition);
    trackHistory.timeStamp.push(timestamp);
}

//======================================Navigation==Properties==============================================
function navProperties()
{
    let waypoint;
    let currWayptDistance=[];
    let min = 0;
    let RemainDist;
    let totalDistWalked = 0;
    
    //==========================================Distances===================================================
    //calculating total distance of the chosen path
    let totalDistance = 0;
    for (let i = 0; i<availablePath[pathIndex].length-1; i++)
    {
        let curPos2 = new google.maps.LatLng(availablePath[pathIndex][i+1].lat,availablePath[pathIndex][i+1].lng);
        let prevPos2 = new google.maps.LatLng(availablePath[pathIndex][i].lat,availablePath[pathIndex][i].lng);
        totalDistance += google.maps.geometry.spherical.computeDistanceBetween(curPos2,prevPos2);
    }
    
    //determining the total distance walked by user
    //using one of trackHistory object, we can practicallt identify total distance walked
    if (trackHistory.latLng.length > 1)
        {
            for (let i=0; i<trackHistory.latLng.length-1; i++)
            {
                totalDistWalked += google.maps.geometry.spherical.computeDistanceBetween(trackHistory.latLng[i+1],trackHistory.latLng[i]);
            }
        }
    
    //Distance between current position to all available waypoints
    for (let i = 0; i <availablePath[pathIndex].length; i++)
    {
        waypoint = new google.maps.LatLng(availablePath[pathIndex][i]);
        currWayptDistance[i] = google.maps.geometry.spherical.computeDistanceBetween(currentPosition,waypoint);
    }

    //Determining the nearest waypoint which is to be the user's next waypoint
    for (let j = 0; j <currWayptDistance.length;j++)
    {
        if (currWayptDistance[min] >= currWayptDistance[j])
            {
                min = j;
            }
    }
    
    //Inform the users if they have reached their destination or their next waypoint if they are in progress
    if(min === availablePath[pathIndex].length-1 && currWayptDistance[min] <= accuracy)
        {
            //Deleting all the info window
            if (infoBox !== null)
            {
                infoBox.setMap(null);
            }
            
            //showing a box showing a sentence indicating that user has arrived to their destination
            window.confirm(" You have reached your destination");
            
            //Stopping the navigation
            navigator.geolocation.clearWatch(id);
            
            //Remaining distance yet to be travelled is 0 when user has arrived
            RemainDist = "0(destination reached)";

	//going back to mainPage.html
            location.href = "index.html" ;
        }
    else
        {
            //closing the previous info window
            if (infoBox !== null)
                {
                    infoBox.setMap(null);
                }
            
            //determining the next waypoint for the user to reach
            coordinates = new google.maps.LatLng(availablePath[pathIndex][min]);

            //printing info window in that particular waypoint
            let boxOptions = {disableAutoPan: true,
                            position: coordinates,
                            content: "NEXT WAYPOINT!!!",
                            alignBottom: true,
                            classBoxURL: "",
                            };
            infoBox = new google.maps.InfoWindow(boxOptions);
            infoBox.open(map);
            
            //Showing the distance between current position to the next waypoint
            RemainDist = currWayptDistance[min].toFixed(2);
        }
    
    //Distance left to reach the destination
    distToDest = (totalDistance-totalDistWalked).toFixed(2);
    
    //If user is out of track for a certain time, the app will reset the calculation of the properties
    if (distToDest < 0)
        {
            alert("sorry! you are out of track. System resetting.");
            totalDistWalked = 0;
            distToDest = (totalDistance-totalDistWalked).toFixed(2);
        }
    
    //Getting the current navigation time (in ms)
    let currentTime = trackHistory.timeStamp[trackHistory.timeStamp.length-1];
    
    //Getting the start navigation time (in ms)
    let startTime = trackHistory.timeStamp[0];

    //Calculating the average speed
    let avgSpeed = (totalDistWalked/((currentTime - startTime)/1000)).toFixed(2);
    
    //Getting estimated time remaining to reach the destination
    let timeRemain = (distToDest / avgSpeed).toFixed(2);
    
    //======================================================================================================
    //============================Displaying==MDL-Grids==in==the==Navigation==page==========================
    //======================================================================================================
    
    //Displaying Distance from the next waypoint
    let displaydistNxtWP = document.getElementById("distNxtWP");
    displaydistNxtWP.innerText = "Distance To Next Waypoint:" + RemainDist + " m";
    
    //Displaying Speed
    let displayAverageSpeed = document.getElementById("avgSpeed");
    displayAverageSpeed.innerHTML = "Average Speed:" + avgSpeed + " m/s";
    
    //Displaying the distance travelled
    let displayDistanceTravelled = document.getElementById("distanceTravelled");
    displayDistanceTravelled.innerHTML = "Distance Travelled:" + totalDistWalked.toFixed(2) + " m";
    
    //Displaying Distance left to reach the destination
    let displayDistanceLeft = document.getElementById("distanceLeft");
    displayDistanceLeft.innerHTML = "Total Distance Left:" + distToDest + " m";
    
    //Displaying Estimated time remaining
    let displayEstimatedTimeRemain = document.getElementById("estimatedTimeRemain");
    displayEstimatedTimeRemain.innerHTML = "Estimated Arrival Time:" + timeRemain + " s";
    
}

//=======================================================================================================
//==========================================Next==Direction==============================================
//=======================================================================================================

//Determine and show the next direction using text and image
function showNextDirection()
{
    let currWayPtHeading = null;
    let angle;
    let TxtDirection = document.getElementById("TxtDirection");
    let ImgDirection = document.getElementById("ImgDirection");
    let direction = "Direction:";
    
    //Calculating the supposed Heading to a certain waypoint
    currWayPtHeading = google.maps.geometry.spherical.computeHeading(currentPosition,coordinates);

    //Calculating the angle the user should turn to go to the desired destination
    angle = currWayPtHeading - prevHeading;
    
    //Setting the parameter for each direction utilizing the angles calculated
    //Printing the corresponding text and images in the navigation page
    if(angle < 5 && angle > -5)
        {
            TxtDirection.innerText = direction + "Go Straight!";
            ImgDirection.src = "images/straight.svg";
        }
    else if (angle < -85 && angle > -95)
        {
            TxtDirection.innerText = direction + "Turn left!";
            ImgDirection.src = "images/left.svg";
        }
    else if (angle > 85 && angle < 95)
        {
            TxtDirection.innerText = direction + "Turn right!";
            ImgDirection.src = "images/right.svg";
        }
    else if (angle < -5 && angle > -85)
        {
            TxtDirection.innerText = direction + "Turn slightly left!";
            ImgDirection.src = "images/slight_left.svg";
        }
    else if (angle > 5 && angle < 85)
        {
            TxtDirection.innerText = direction + "Turn slightly right!";
            ImgDirection.src = "images/slight_right.svg";
        }
    else{
        TxtDirection.innerText = direction + "Make a U-turn!";
        ImgDirection.src = "images/uturn.svg";
        }
}

//======================================================================================================
//===================================End==Of==Navigation==Page==========================================
//======================================================================================================
