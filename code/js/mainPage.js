/*
 *ENG1003 Assigment 2 (2018 S1)
 *Team 205
 *Members: Ngo Jin Yi 28654390
 *         Carwen Wong Kah Vun 29032083
 *         Jose Fernando 29062950
 *         Giridhar Gopal Sharma 29223709
 *
 *Walking Navigation App
 *Aim: This javascript file is designed to initialise the server path and user path so that there will be multiple paths shown that can be chosen by the user. An add button is *     also added to direct the user to the addRoute page to add paths and store as user path.          
 *Functions included in this file:
 *  -initMap()
 *  -jsonpRequest(link,data)
 *  -storeRoute(result)
 *  -retrieveDataAndDisplay()
 *  -viewPath(pathIndex,pathType)
 *Last Modified: 19 May 2018
 */

//============================================================================
//================================Main==Page==================================
//============================================================================

//defining URL components
let URL="https://eng1003.monash/api/campusnav/";
let routeData = 
    {
        campus:"sunway",
        callback:"storeRoute"
    };

//initializing some variables
let pathArray = [];
let pathListElementServer = document.getElementById('path-list-server');
let pathListElementUser = document.getElementById('path-list-user');
let pathListPDO;
let n=0;

//executing jsonpRequest function using the callback function initMap
function initMap()
{
    jsonpRequest(URL,routeData);
    let pathJSON = null;
    let pathListPDOTest = null;
    let pathListPDO = null;
}

//jsonpRequest is a function to build the complete URL and its script elements
function jsonpRequest(link, data)
{
    //Build URL parameters from data object.
    let para = "";
    
    //For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (para.length === 0)
            {
                // First parameter starts with '?'
                para += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                para += "&";
            }
            
            //encoding key and its value
            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);
            
            //defining the complete parameter
            para += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');3
    script.src = link + para;
    document.body.appendChild(script);
}

//storing the data from the web service into local storage using a call back function defined in web server
function storeRoute(result)
{
    //Testing to examine whether users define their own routes
    let test = localStorage.getItem(storageKey);
    let route = result;
    
    if (test === null)
    {
        //creating path instance for each path extracted from web server
        let pathServer1 = route[0];
        let pathServer2 = route[1];
        let pathServer = [pathServer1,pathServer2];
        
        //if user does not add their own routes, then we set the user-defined property as null
        let pathList = [pathServer,null];
        
        //storing item to local storage
        if(typeof(Storage) !== undefined)
            {
                let routeSerialise = JSON.stringify(pathList);
                localStorage.setItem(storageKey,routeSerialise);
                retrieveDataAndDisplay();
            }
        else
            {
                console.log("Storage is not defined");
            }
    }
    //if user defines their own routes, this means that local storage is already defined
    else
    {
        retrieveDataAndDisplay();
    }
}

//==========================================================================================
//=============================Displaying==Options==In==Main==Page==========================
//==========================================================================================

function retrieveDataAndDisplay()
{
    //retrieving data from local storage
    if (typeof(Storage) !== "undefined")
    { 
        let pathJSON = localStorage.getItem(storageKey);
        let pathListPDOTest = JSON.parse(pathJSON);
        let listHTMLServer = "";
        let listHTMLUser = "";
        //looping data to extract as an array of either server paths or user-defined path
        for( let i = 0; i< pathListPDOTest.length ; i++)
        {
            pathListPDO = pathListPDOTest[i];
            let pathInstanceArray = [];
            
            if(pathListPDO !== null)
                {  
                    //another looping to examine every single array inside the path array                    
                    for( let m = 0; m < pathListPDO.length ; m++ )
                    {
                        //Initialize the path class 
                        let newPath = new Path();
                        newPath.initializeFromStorage(pathListPDO[m]); 
                            
                        //push pathInstanceArray 
                        pathInstanceArray.push(newPath) ;
                        
                        if (i===0)
                            {
                                listHTMLServer += "<tr> <td onmousedown=\"viewPath("+m+ "," + i +")\" class=\"full-width mdl-data-table__cell--non-numeric\">"+ newPath.title ;
                                listHTMLServer += "<div class=\"subtitle\">" + newPath.calcDistance+ "meters, " + newPath.noOfTurns() + "turns" +"</div></td></tr>"; 
                            }
                        else
                            {
                                listHTMLUser += "<tr> <td onmousedown=\"viewPath("+m+ ","+ i + ")\" class=\"full-width mdl-data-table__cell--non-numeric\">"+ newPath.title;
                                listHTMLUser += "<div class=\"subtitle\">" + newPath.calcDistance+ "meters, " + newPath.noOfTurns() + "turns" +"</div></td></tr>";
                            }
                    } 
                }  
        }
        pathListElementServer.innerHTML = listHTMLServer;
        if (listHTMLUser !== "")
            {
                pathListElementUser.innerHTML = listHTMLUser;
            }
    }
    else 
    {
        console.log("Error : local storage is not found!!!!! :(")
    }
}

//==========================================================================================
//=====================================View==Path===========================================
//==========================================================================================

function viewPath(pathIndex,pathType)
{   
    //storing the index of the path array that is chosen by users
    let pathChosen = [pathType,pathIndex];
    localStorage.setItem(storageKey+"-selected", JSON.stringify(pathChosen));
    
    //directing main page to navigation page
    location.href = "navigate.html" ; 
}

//============================================================================================================
//======================================End==Of==Main=Page====================================================
//============================================================================================================



