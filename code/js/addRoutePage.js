/*
 *ENG1003 Assigment 2 (2018 S1)
 *Team 205
 *Members: Ngo Jin Yi 28654390
 *         Carwen Wong Kah Vun 29032083
 *         Jose Fernando 29062950
 *         Giridhar Gopal Sharma 29223709
 *
 *Walking Navigation App
 *Aim: This javascript file is designed to create the add route function to let the users of the app to create their own routes in the walking navigation app.
 *Functions included in this file:
 *  -initMap(position)
 *  -addMarker()
 *  -getMarkerPosition()
 *  -undoMarker()
 *  -checkRouteName(routeInput)
 *  -saveUserRoute()
 *  -storeUserRoute(routeObject)
 *Last Modified: 19 May 2018
 */

//=================================================================================
//================================Add==Route==Page=================================
//=================================================================================

//initializing variables
let currPos ={
    lat:null,
    lng:null
};
let markerNumber = 0;
let posArray = [];
let polylineArray = []; 
let marker1;
let marker2=[];
let newMarker = null;

//=================================================================================
//==================================Map==Display===================================
//=================================================================================

//determining the centre of the map
let center = navigator.geolocation.getCurrentPosition(initMap);

//calling initMap callback function to display google map
function initMap(position) 
{
        //getting the current location of the user and setting it as the first position
        currPos.lat=position.coords.latitude;
        currPos.lng=position.coords.longitude;
           
        map = new google.maps.Map(document.getElementById('map'), 
        {
          zoom: 18,
          center:
            {
            lat: currPos.lat,
            lng: currPos.lng
            },
            disableDefaultUI: true
        });
}

//adding marker to the user's current position in the map
function addMarker()
{   
     if (newMarker === null)
     {
         //use draggable marker to indicate the first position 
         newMarker = new google.maps.Marker({
             position:{
                lat: currPos.lat,
                lng: currPos.lng
             },
             map: map,
             draggable:true ,
             animation: google.maps.Animation.BOUNCE,
         });
     }
    //if marker is already displayed on the map, no marker will be displayed anymore
    else
    {
        //displaying a message to indicate that the user has already had a marker to use
        alert("you already have your first waypoint");
    }
    
}

//=========================================================================================================
//====================================Setting==User==Defined==Path=========================================
//=========================================================================================================

//getMarkerPosition function is to set the path line and markers along every waypoint that the user defines
function getMarkerPosition()
{
    //obtaining the first marker position
    newMarkerPosition = newMarker.getPosition();
    
    //setting the coordinates of the marker position
    newMarkerLat = newMarkerPosition.lat() ;
    newMarkerLng = newMarkerPosition.lng() ;
    newMarkerCoordinates = {
        lat:newMarkerLat,
        lng:newMarkerLng
        };
    
    //inserting the particular coordinates into an array 
    //this array is used to store the history of all the selected waypoints by the user
    posArray.push(newMarkerCoordinates);
    
    //setting a "green-colour" marker as the first position set by the user
    if (markerNumber === 0)
        {
            marker1 = new google.maps.Marker({
                map : map,
                position:
                        {
                            "lat": newMarkerLat,
                            "lng": newMarkerLng
                        },
                labelOrigin: new google.maps.Point(15, 10),
                icon:
                    {
                        url: 'http://maps.google.com/mapfiles/ms/icons/green.png',
                    }
            });
            markerNumber++ ;
        }
    else if (markerNumber >= 1 )
        {
            //making condition for determining the following waypoints
            //informing the user to change the chosen waypoint if it is the same as the previous waypoint's position
            if (posArray[markerNumber].lat === posArray[markerNumber-1].lat && posArray[markerNumber].lng === posArray[markerNumber-1].lng)
                {
                    alert("Waypoint is the same! Add another waypoint!");
                    posArray.pop();
                    polylineArray.pop();
                }
            else
                {
                    //updating the last waypoint as "pink-colour" marker
                    //since the app does not know when users will stop adding waypoints,
                    //the app will assume that every new waypoint after the first one is the last waypoint
                    marker2[markerNumber] = new google.maps.Marker({
                        map : map,
                        position:{
                            "lat": newMarkerLat,
                            "lng": newMarkerLng
                        },
                        labelOrigin: new google.maps.Point(15, 10),
                        icon:{
                            url: 'http://maps.google.com/mapfiles/ms/icons/pink.png',
                        }
                    });
                    markerNumber++ ;
                }
            
            //when user add other waypoints after 2, 
            //the app will start replacing the markers after the 1st and before the last waypoints as "blue" colour
            for (let i =1; i< markerNumber-1; i++)
            {
                marker2[i].setMap(null);
                marker2[i] = new google.maps.Marker({
                    map : map,
                    position:{
                        "lat": posArray[i].lat,
                        "lng": posArray[i].lng
                    },
                    labelOrigin: new google.maps.Point(15, 10),
                    icon:{
                        url: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                    }
                });
            }
        }
    
        //creating polyline withing the waypoints
        let userPathLine = new google.maps.Polyline({
            path: [posArray[posArray.length - 2],posArray[posArray.length - 1]],
            geodesic: true,
            strokeColor: '#000000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        //Forming an array to register the polylines constructed
        polylineArray.push(userPathLine);

        //Displaying the polylines created on the map
        polylineArray[posArray.length - 1].setMap(map);
}

//undoMarker function deletes the last waypoint's marker and pathline
function undoMarker()
{
    //first position that user defined is fixed
    //if user attempts to undo it, an information box will be displayed
    if (markerNumber === 1)
        {
            alert("Fixed first position can not be undo!");
        }
    else
        {
            //Eradicating the last pathline
            polylineArray[posArray.length - 1].setMap(null);

            //remove the last coordinates and polyline data
            posArray.pop();
            polylineArray.pop(); 

            //clearing the last marker
            marker2[markerNumber-1].setMap(null);
            markerNumber = markerNumber-1;
            
            //replacing the new "last" marker as pink marker which indicates the "last" waypoint
            marker2[markerNumber-1].setMap(null);
            marker2[markerNumber-1] = new google.maps.Marker({
                map : map,
                position:{
                    "lat": posArray[markerNumber-1].lat,
                    "lng": posArray[markerNumber-1].lng
                },
                labelOrigin: new google.maps.Point(15, 10),
                icon:{
                    url: 'http://maps.google.com/mapfiles/ms/icons/pink.png'
                }
            });
        }
}

//=================================================================================================
//==========================Saving==Path==And==Storing==The==Data==In==Local==Storage==============
//=================================================================================================

//checkRouteName function is to check whether or not the route title the user enters valid
function checkRouteName(routeInput)
{
    let routeNameSize = routeInput.length;
    
    //some aspects that make a title invalid
    if(routeNameSize<2 || routeInput.includes("  ") === true || routeInput === " ")
        {
            routeInput = null;
        }
    
    return routeInput;
}
 
//saveUserRoute function is a function that is executed when users save their defined path
function saveUserRoute()
{
    //if the waypoints defined is less than 2 meaning it is unnecessary to use this app,
    //an information box will come out asking user to add more waypoints
    if (markerNumber<3)
        {
            alert("Need more than 2 waypoints !!");
        }
    else
        {
            //When waypoints are valid
            //Prompt the users to enter the names of their routes 
            let routeName = prompt("Please enter the name of the route :)");
            
            //calling the checking function
            routeName = checkRouteName(routeName);
            
            //if the title is not valid then the app will prompt the user to add a valid name
            if (routeName === null)
                {
                    while (routeName === null)
                    {
                        routeName = prompt("Please enter the name of the route correctly :)");
                        routeName = checkRouteName(routeName);
                    }
                }
            else if (routeName !== null)
                {
                    //when the title defined is valid, all the path properties will be stored as an object
                    let routeObject ={
                        title: routeName,
                        locations: posArray,
                        prerecordedRoutesIndex: null
                    };

                   //calling the next funtion
                   storeUserRoute(routeObject);
                }
        }
}
 
//storeUserRoute function is to store the route properties object inside the local storage
//local storage used is the same as the local storage to keep server paths
function storeUserRoute(routeObject)
{
    //extracting the existing item from the local storage
    //this is to prevent overwriting the local storage 
    //as the app is using the same local storage for both server and user-defined path
    let pathJSON = localStorage.getItem(storageKey);
    let pathListPDO = JSON.parse(pathJSON);
    
    //if user defined item has not existed, the app stores the route object inside
    if(pathListPDO[1] === null)
        {
            let test = [];
            test.push(routeObject);
            pathListPDO[1] = test;

            let userRouteSerialise = JSON.stringify(pathListPDO);
            localStorage.setItem(storageKey,userRouteSerialise);
        }
    //if user defined item has already existed, then the app adds the route object into the user array
    else if(pathListPDO[1] !== null)
        {
            pathListPDO[1].push(routeObject);
            let userRouteSerialise = JSON.stringify(pathListPDO);
            localStorage.setItem(storageKey,userRouteSerialise);
        }
    
    //deleting all defined pathlines and markers back from the map after users have successfully saved their routes
    //this is done to give the chance for users to draw another path without needing to refresh the page
    marker1.setMap(null);
    newMarker.setMap(null);
    for (let i=0; i<polylineArray.length; i++)
        {
            polylineArray[i].setMap(null);
        }
    for (let i=1; i<marker2.length; i++)
        {
            marker2[i].setMap(null);
        }
    
    //setting all the global variables back to its initial condition
    //this is done so that a new set of route-adding process can be done
    currPos ={
                lat:null,
                lng:null
            };
    marker1 = null;
    newMarker = null;
    markerNumber = 0;
    posArray = [];
    polylineArray = []; 
    marker2=[];

    //initializing the page so that all functionalities can be executed
    location.href = "addRoute.html";
}

//============================================================================================================
//======================================End==Of==Add=Route=Page===============================================
//============================================================================================================
