/*
 *ENG1003 Assigment 2 (2018 S1)
 *Team 205
 *Members: Ngo Jin Yi 28654390
 *         Carwen Wong Kah Vun 29032083
 *         Jose Fernando 29062950
 *         Giridhar Gopal Sharma 29223709
 *
 *Walking Navigation App
 *Aim: This javascript file is designed to create two classes that will be called in other javascript files.
 *Classes included in this file:
 *  -Path
 *  -Pathlist
 *Last Modified: 19 May 2018
 */

//============================================================================
//================================Shared======================================
//============================================================================

//defining storage key to be used for local Storage
let storageKey="ENG1003Routes";

// Path Class
class Path
    {
        constructor()
        {
            // Private attributes:
            this._locations = [];
            this._title = "";
            this._preRecordedRoutesIndex = null;
            this._calcDistance;
        }
        
        //Public methods
        
        // This function sets the title of the path as a string.
        set title(newTitle)
        {
            // Make sure that is only string format 
            if(typeof(newTitle) === "string")
                {
                    this._title = newTitle;
                }
            else
                {
                   alert("This title is not a string");   
                }
        }
        
        // This function sets the locations of the path as an array.
        set locations(newLocations)
        {
            this._locations = newLocations;
        }
        
        // This function returns locations of the path as an array.
        get locations()
        {
            return this._locations;
        }
        
        // This function returns the title of the path as a string.
        get title()
        {
            return this._title;
        }
        
        // This function returns the preRecordedRoutesIndex.
        get preRecordedRoutesIndex()
        {
            return this._preRecordedRoutesIndex;
        }
        
        // This function returns the distance remained.
        get calcDistance()
        {
            let remainDistance = 0;
            for (let i = 1; i<this._locations.length; i++)
                {
                    let check1 = new google.maps.LatLng(this._locations[i-1]);
                    let check2 = new google.maps.LatLng(this._locations[i]);
                    remainDistance += google.maps.geometry.spherical.computeDistanceBetween(check1,check2);
                }
            return remainDistance.toFixed(2);
        }
        
        // Reinitialises this instance from a public-data path object.
        initializeFromStorage(PDO)
        {
            // Initialise the instance via the mutator methods from the PDO object.
            this._locations = PDO.locations;
            this._title = PDO.title;
            this._preRouteIndex = PDO.preRecordedRoutesIndex;
        }
        
        // This function returns the number of turns.
        noOfTurns()
        {
            let turn = this._locations.length-2;
            return turn;
        }
    }
        

// Pathlist Class
class Pathlist
    {
        constructor()
        {
            //Private attributes:
            this._category =[];
            this._pathArr = [];
        }
        
        // Code to initialise Path array
    
        // Public method
        
        // This function returns the category as an array.
        get category()
        {
            return this._category;
        }
        
        // This function returns the locations inside the path as an array.
        get pathArr()
        {
            return this._pathArr;
        }
        
        // Reinitialises this instance from a public-data pathlist object.
        initializeFromPathlistPDO(pathListPDO,routeIndex)
        {
            if (typeof(Storage)!==undefined)
            {
                for(let i=0; i<pathListPDO[routeIndex].length;i++)
                {
                    let newPath = new Path;
                    newPath.initializeFromStorage(pathListPDO[routeIndex][i]);
                    this._pathArr[i]=newPath.locations;
                }
                if (routeIndex === 0)
                {
                    this._category = "server";
                }
                else
                {
                    this._category = "user-defined";
                }
            }
        }
    }

//============================================================================================================
//======================================End==Of==Shared=======================================================
//============================================================================================================
